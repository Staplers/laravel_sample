-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `bets`;
CREATE TABLE `bets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bettype` varchar(100) NOT NULL,
  `competition_id_1` int(11) NOT NULL,
  `competition_id_2` int(11) DEFAULT NULL,
  `sport_id_1` int(11) NOT NULL,
  `sport_id_2` int(11) DEFAULT NULL,
  `dateandtime_1` varchar(100) NOT NULL,
  `dateandtime_2` int(11) DEFAULT NULL,
  `whosplaying_id_1` int(11) DEFAULT NULL,
  `whosplaying_id_2` int(10) DEFAULT NULL,
  `tiptype_id_1` int(11) NOT NULL,
  `tiptype_id_2` int(11) DEFAULT NULL,
  `betcat_id_1` int(11) DEFAULT NULL,
  `betcat_id_2` int(11) DEFAULT NULL,
  `actualtip_1` varchar(100) DEFAULT NULL,
  `actualtip_2` varchar(100) DEFAULT NULL,
  `odds` double DEFAULT NULL,
  `trustnote` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `competition_id_1` (`competition_id_1`),
  KEY `competition_id_2` (`competition_id_2`),
  KEY `sport_id_2` (`sport_id_2`),
  KEY `dateandtime_2` (`dateandtime_2`),
  KEY `whosplaying_id_1` (`whosplaying_id_1`),
  KEY `tiptype_id_1` (`tiptype_id_1`),
  KEY `tiptype_id_2` (`tiptype_id_2`),
  KEY `whosplaying_id_2` (`whosplaying_id_2`),
  KEY `sport_id_1` (`sport_id_1`),
  KEY `betcat_id_1` (`betcat_id_1`),
  KEY `betcat_id_2` (`betcat_id_2`),
  CONSTRAINT `bets_ibfk_1` FOREIGN KEY (`competition_id_1`) REFERENCES `competition` (`id`),
  CONSTRAINT `bets_ibfk_10` FOREIGN KEY (`sport_id_2`) REFERENCES `sport` (`id`),
  CONSTRAINT `bets_ibfk_12` FOREIGN KEY (`whosplaying_id_1`) REFERENCES `whosplaying` (`id`),
  CONSTRAINT `bets_ibfk_13` FOREIGN KEY (`tiptype_id_1`) REFERENCES `tiptype` (`id`),
  CONSTRAINT `bets_ibfk_14` FOREIGN KEY (`tiptype_id_2`) REFERENCES `tiptype` (`id`),
  CONSTRAINT `bets_ibfk_15` FOREIGN KEY (`whosplaying_id_2`) REFERENCES `whosplaying` (`id`),
  CONSTRAINT `bets_ibfk_16` FOREIGN KEY (`sport_id_1`) REFERENCES `sport` (`id`),
  CONSTRAINT `bets_ibfk_17` FOREIGN KEY (`betcat_id_1`) REFERENCES `bet_category` (`id`),
  CONSTRAINT `bets_ibfk_18` FOREIGN KEY (`betcat_id_2`) REFERENCES `bet_category` (`id`),
  CONSTRAINT `bets_ibfk_2` FOREIGN KEY (`competition_id_2`) REFERENCES `competition` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `bet_category`;
CREATE TABLE `bet_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `competition`;
CREATE TABLE `competition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `membership`;
CREATE TABLE `membership` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(100) NOT NULL,
  `plan_id` int(100) NOT NULL,
  `status` varchar(255) NOT NULL,
  `amount` double NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `membership` (`id`, `user_id`, `plan_id`, `status`, `amount`, `start_date`, `end_date`, `created_at`, `updated_at`) VALUES
(1,	52,	2,	'active',	58.9,	'2019-08-28',	'2019-11-28',	'2019-08-30 10:55:27',	'2019-08-28 06:33:28');

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_no` char(250) NOT NULL,
  `user_id` int(11) NOT NULL,
  `actual_amount` float(10,2) NOT NULL,
  `paid_amount` float(10,2) NOT NULL,
  `payment_for` text NOT NULL,
  `status` enum('Pending','Failed','Paid') NOT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `gateway` char(250) DEFAULT NULL,
  `mode` enum('Online','Offline') NOT NULL DEFAULT 'Online',
  `transaction_id` varchar(500) DEFAULT NULL,
  `paypal_payerid` varchar(500) NOT NULL DEFAULT '0',
  `subscription_id` varchar(500) DEFAULT NULL,
  `gateway_customer_id` varchar(500) DEFAULT NULL,
  `card_last_four` char(4) DEFAULT NULL,
  `card_name` char(250) DEFAULT NULL,
  `billing_address` varchar(500) DEFAULT NULL,
  `billing_city` char(250) DEFAULT NULL,
  `billing_state` char(250) DEFAULT NULL,
  `billing_country` char(250) DEFAULT NULL,
  `billing_phone` char(100) DEFAULT NULL,
  `gateway_json_response` text,
  `failed_message` varchar(500) DEFAULT NULL,
  `start_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `payment_mode`;
CREATE TABLE `payment_mode` (
  `paymentModeId` int(11) NOT NULL AUTO_INCREMENT,
  `gateway` enum('Stripe','Paypal','Authorize.net','Klik') NOT NULL,
  `merchant_id` varchar(500) DEFAULT NULL COMMENT 'for paypal',
  `production_url` varchar(500) DEFAULT NULL,
  `sandbox_url` varchar(500) DEFAULT NULL,
  `api_username` varchar(500) DEFAULT NULL COMMENT 'for paypal',
  `api_password` varchar(500) DEFAULT NULL COMMENT 'for paypal',
  `api_signature` varchar(500) DEFAULT NULL COMMENT 'for paypal',
  `public_key` varchar(500) DEFAULT NULL COMMENT 'for stripe',
  `secret_key` varchar(500) DEFAULT NULL COMMENT 'for stripe',
  `api_login_id` varchar(500) DEFAULT NULL COMMENT 'for authorize.net',
  `transaction_key` varchar(500) DEFAULT NULL COMMENT 'for authorize.net',
  `status` enum('Active','Inactive') NOT NULL,
  `account_type` enum('Live','Test') NOT NULL,
  `notification_email` char(250) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`paymentModeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `payment_mode` (`paymentModeId`, `gateway`, `merchant_id`, `production_url`, `sandbox_url`, `api_username`, `api_password`, `api_signature`, `public_key`, `secret_key`, `api_login_id`, `transaction_key`, `status`, `account_type`, `notification_email`, `created_at`, `updated_at`) VALUES
(2,	'Stripe',	NULL,	NULL,	NULL,	'',	'',	'',	'pk_test_E28b7nsIh9i0dbmewtjZJOzd',	'sk_test_8qefQH5fHrXLs7ChNkAT2g0O',	'',	'',	'Active',	'Test',	'david@bedadstrong.com',	'2019-01-25 12:30:28',	'2019-01-25 12:30:28'),
(3,	'Paypal',	NULL,	NULL,	NULL,	'gourav_api1.staplelogic.in',	'EK5XNLCLPVEVUHJX',	'ArgX1JoEl.LNTJ5nkTmgzkfLaDiVAxKMXUK0FpdxFVn1kDN2O33z6Q8z',	'',	'',	'',	'',	'Active',	'Test',	'paypal@gmail.com',	'2019-01-25 13:17:48',	'2019-01-25 13:17:48'),
(4,	'Authorize.net',	NULL,	NULL,	NULL,	'',	'',	'',	'',	'',	'6VbFKwY93tV',	'6gm9X6R384aGYr4x',	'Active',	'Test',	'authorize@gmail.com',	'2019-02-21 11:44:58',	'2019-02-21 06:14:58'),
(5,	'Klik',	NULL,	NULL,	NULL,	'',	'',	'',	'',	'BE9EE64A-C98A-11E9-83EB-A52B7DFC7FFD',	'6VbFKwY93tV',	'6gm9X6R384aGYr4x',	'Active',	'Test',	'authorize@gmail.com',	'2019-08-28 12:00:30',	'2019-02-21 06:14:58');

DROP TABLE IF EXISTS `plans`;
CREATE TABLE `plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `duration` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `stats` text,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `plans` (`id`, `name`, `amount`, `duration`, `status`, `stats`, `created_at`, `updated_at`) VALUES
(1,	'Month To Month',	29.90,	'1 Month',	'active',	'<li><p>Exclusive Members ONLY access</p></li>\r\n                                    <li><p>E-mail notifications</p></li>\r\n                                    <li><p>DAILY BANG predictions</p></li>\r\n                                    <li><p>ACCA OF THE DAY predictions</p></li>\r\n                                    <li><p>BOMBS OF THE DAY predictions</p></li>',	'2019-08-27 04:07:58',	NULL),
(2,	'3 Month',	58.90,	'3 Month',	'active',	' <li><p>Exclusive Members ONLY access</p></li>\r\n                                    <li><p>E-mail notifications</p></li>\r\n                                    <li><p>DAILY BANG predictions</p></li>\r\n                                    <li><p>ACCA OF THE DAY predictions</p></li>\r\n                                    <li><p>BOMBS OF THE DAY predictions</p></li>',	'2019-08-27 12:25:43',	NULL),
(3,	'1 Year',	159.90,	'1 Year',	'active',	' <li><p>Exclusive Members ONLY access</p></li>\r\n                                    <li><p>E-mail notifications</p></li>\r\n                                    <li><p>DAILY BANG predictions</p></li>\r\n                                    <li><p>ACCA OF THE DAY predictions</p></li>\r\n                                    <li><p>BOMBS OF THE DAY predictions</p></li>',	'2019-08-27 04:08:28',	NULL);

DROP TABLE IF EXISTS `player`;
CREATE TABLE `player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `review`;
CREATE TABLE `review` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` varchar(100) NOT NULL,
  `star` int(11) DEFAULT NULL,
  `is_approved` varchar(100) DEFAULT NULL,
  `comment` tinytext,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `sport`;
CREATE TABLE `sport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `team`;
CREATE TABLE `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tiptype`;
CREATE TABLE `tiptype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(2) DEFAULT '2',
  `status` enum('active','deactive','pending') COLLATE utf8mb4_unicode_ci DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users` (`id`, `name`, `last_name`, `email`, `email_verified_at`, `password`, `remember_token`, `role_id`, `status`, `created_at`, `updated_at`) VALUES
(1,	'Admin',	NULL,	'admin@gmail.com',	NULL,	'$2y$10$PSiXQsdAcafHwWqzsPWyneILXXEA/y2LYojlmyl9z.bCRmMXP7apS',	'k1nXHnf1WsSv6nFI7Q5dOtbRcJjXyZajV8A4jgRv34he0findVKxJ4smoaH9',	1,	'pending',	NULL,	NULL),
(52,	'Gurpreet Puri',	NULL,	'user@gmail.com',	NULL,	'$2y$10$PSiXQsdAcafHwWqzsPWyneILXXEA/y2LYojlmyl9z.bCRmMXP7apS',	'fKy9Is9sXCkwymgppY52Msvp4rpluEpEnE8WAf2sujQDoLe2upwHX8bg5A83',	1,	'active',	NULL,	NULL);

DROP TABLE IF EXISTS `whosplaying`;
CREATE TABLE `whosplaying` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  `teama` int(11) DEFAULT NULL,
  `teamb` int(11) DEFAULT NULL,
  `playera` int(11) DEFAULT NULL,
  `playerb` int(11) DEFAULT NULL,
  `status` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `teama` (`teama`),
  KEY `teamb` (`teamb`),
  KEY `playera` (`playera`),
  KEY `playerb` (`playerb`),
  CONSTRAINT `whosplaying_ibfk_1` FOREIGN KEY (`teama`) REFERENCES `team` (`id`),
  CONSTRAINT `whosplaying_ibfk_10` FOREIGN KEY (`teamb`) REFERENCES `team` (`id`),
  CONSTRAINT `whosplaying_ibfk_11` FOREIGN KEY (`playera`) REFERENCES `player` (`id`),
  CONSTRAINT `whosplaying_ibfk_12` FOREIGN KEY (`playerb`) REFERENCES `player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2019-08-30 12:54:55
