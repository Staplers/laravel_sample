<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



  //language route here
  Route::get('/en', 'HomeController@changeLanguage')->defaults('locale','en');
  Route::get('/ro', 'HomeController@changeLanguage')->defaults('locale','ro');
  Route::get('/de', 'HomeController@changeLanguage')->defaults('locale','de');

  //Clear Cache facade value:
  Route::get('/clear-cache', function() {
      $exitCode = Artisan::call('cache:clear');
      return '<h1>Cache facade value cleared</h1>';
  });

  //Reoptimized class loader:
  Route::get('/optimize', function() {
      $exitCode = Artisan::call('optimize');
      return '<h1>Reoptimized class loader</h1>';
  });

  //Route cache:
  Route::get('/route-cache', function() {
      $exitCode = Artisan::call('route:cache');
      return '<h1>Routes cached</h1>';
  });

  //Clear Route cache:
  Route::get('/route-clear', function() {
      $exitCode = Artisan::call('route:clear');
      return '<h1>Route cache cleared</h1>';
  });


  //Clear Config cache:
  Route::get('/config-clear', function() {
      $exitCode = Artisan::call('config:clear');
      return '<h1>Route cache cleared</h1>';
  });

  //Clear View cache:
  Route::get('/view-clear', function() {
      $exitCode = Artisan::call('view:clear');
      return '<h1>View cache cleared</h1>';
  });

  
  Auth::routes();

//AddContent Controller Routes
  Route::get('addcontent/sport','AddContentController@addSport');   
  Route::post('addcontent/sport/save','AddContentController@sportSave');   
  Route::post('addcontent/sport/remove','AddContentController@removesport');   
  Route::get('addcontent/sport/edit/{aid}','AddContentController@editsport');   
  Route::post('addcontent/sport/edit/save','AddContentController@editSavesport');
  Route::get('addcontent/competition','AddContentController@addCompetition');   
  Route::get('addcontent/player','AddContentController@addplayer');   
  Route::post('addcontent/player/save','AddContentController@playerSave');   
  Route::post('addcontent/player/remove','AddContentController@removeplayer');   
  Route::get('addcontent/player/edit/{aid}','AddContentController@editplayer');  
  Route::get('addcontent/team','AddContentController@addteam');   
  Route::post('addcontent/team/save','AddContentController@teamSave');   
  Route::post('addcontent/team/remove','AddContentController@removeteam');   
  Route::get('addcontent/team/edit/{aid}','AddContentController@editteam');   
  Route::post('addcontent/team/edit/save','AddContentController@editSaveteam'); 
  Route::get('addcontent/tiptype','AddContentController@addtiptype');   
  Route::post('addcontent/tiptype/save','AddContentController@tiptypeSave');   
  Route::post('addcontent/tiptype/remove','AddContentController@removetiptype'); 
  Route::get('addcontent/tiptype/edit/{aid}','AddContentController@edittiptype');
  Route::get('addcontent/plan','AddContentController@addplan');
  Route::post('addcontent/plan/save','AddContentController@planSave'); 
  Route::post('addcontent/plan/remove','AddContentController@removeplan'); 
  Route::get('addcontent/plan/edit/{aid}','AddContentController@editplan'); 
  Route::post('addcontent/plan/edit/save','AddContentController@editSaveplan');
  Route::get('addcontent/betcategory','AddContentController@addbetcategory');
 
/*Review Controller Routes*/
 Route::get('review/allreview','ReviewController@review');
 Route::post('review/changeStatus','ReviewController@changeStatus');
 Route::post('review/deleteReview','ReviewController@deleteReview');
 Route::get('review/edit/{rid}','ReviewController@editReview');
 Route::post('review/edit/save','ReviewController@editReviewSave');
 Route::any('review/add','ReviewController@create');

/* Template Controller Routes */
  Route::get('template/alltemplate','TemplateController@getAll');
  Route::get('template/addnew','TemplateController@addnew');
  Route::post('template/addnew/save','TemplateController@addnewSave');
  Route::post('template/deletetemplate','TemplateController@deletetemplate');
  Route::get('template/edit/{rid}','TemplateController@editTemplate');
  Route::post('template/edit/save','TemplateController@editSave');